import time

from merico.analysis.analysis import Analysis
from merico.analysis.analyzer.aspect_function_analyzer import AspectFunctionAnalyzer
from merico.analysis.analyzer.cherry_pick_analyzer import CherryPickAnalyzer
from merico.analysis.analyzer.commit_db_analyzer import CommitDbAnalyzer
from merico.analysis.analyzer.commit_equivalent_weight_analyzer import CommitEquivalentWeightAnalyzer
from merico.analysis.analyzer.commit_function_analyzer import CommitFunctionAnalyzer
from merico.analysis.analyzer.commit_ccg_function_analyzer import CommitCcgFunctionAnalyzer
from merico.analysis.analyzer.doc_coverage_semantic_analyzer import DocCoverageSemanticAnalyzer
from merico.analysis.analyzer.dryness_analyzer import DrynessAnalyzer
from merico.analysis.analyzer.multi_ccg_analyzer import MultiCcgAnalyzer
from merico.analysis.analyzer.project_call_graph_analyzer import ProjectCallGraphAnalyzer
from merico.analysis.analyzer.commit_non_ccg_file_analyzer import CommitNonCcgFileAnalyzer
from merico.analysis.analyzer.static_test_coverage_analyzer import StaticTestCoverageAnalyzer
from merico.analysis.concern.workload_observer import WorkloadObserver
from merico.analysis.analyzer.commit_type_analyzer import CommitTypeAnalyzer
from merico.analysis.analyzer.commit_author_detector_analyzer import CommitAuthorDetectorAnalyzer
from merico.analysis.project import Project
from merico.analysis.report.report_helper import ReportTag
from merico.analysis.report.report_operator import ReportWriter
from merico.util.inspector import inspector
from merico.util.logger import init_log_config

# GIT_URL = 'https://gitlab.com/meri.co/code-analytics/test-repos/ugs-merge-commit-touch-files.git'
# GIT_URL = 'https://gitlab.com/meri.co/code-analytics/test-repos/0528_github_java.git'
# GIT_URL = 'https://github.com/optimizely/java-sdk.git'
# GIT_URL = 'https://github.com/uber/cadence'
GIT_URL = 'https://github.com/gliderlabs/ssh.git'
# GIT_URL = 'https://github.com/shinnytech/tqsdk-python'

INSPECTOR_FILENAME = 'run_ccg'


def run_ccg():
    project = Project(GIT_URL)
    project.update()

    analysis = Analysis(project)
    workload_observer = WorkloadObserver()
    commit_db_analyzer = CommitDbAnalyzer(with_process_unit=True, dump=True)
    commit_author_analyzer = CommitAuthorDetectorAnalyzer()
    cherry_pick_analyzer = CherryPickAnalyzer()
    commit_type_analyzer = CommitTypeAnalyzer()
    commit_equivalent_weight_analyzer = CommitEquivalentWeightAnalyzer()
    multi_ccg_analyzer = MultiCcgAnalyzer()

    commit_ccg_function_analyzer = CommitCcgFunctionAnalyzer()
    commit_non_ccg_file_analyzer = CommitNonCcgFileAnalyzer()
    commit_function_analyzer = CommitFunctionAnalyzer()
    aspect_function_analyzer = AspectFunctionAnalyzer()
    doc_coverage_semantic_analyzer = DocCoverageSemanticAnalyzer()
    dryness_analyzer = DrynessAnalyzer()
    project_call_graph_analyzer = ProjectCallGraphAnalyzer()
    static_test_coverage_analyzer = StaticTestCoverageAnalyzer()
    report_writer = ReportWriter(tag=ReportTag.CCG)
    workload_observer.register_to(analysis)
    commit_db_analyzer.register_at_begin(analysis)
    commit_author_analyzer.register_to(analysis)
    cherry_pick_analyzer.register_to(analysis)
    commit_type_analyzer.register_to(analysis)
    commit_equivalent_weight_analyzer.register_to(analysis)
    multi_ccg_analyzer.register_to(analysis)
    commit_ccg_function_analyzer.register_to(analysis)
    commit_non_ccg_file_analyzer.register_to(analysis)
    commit_function_analyzer.register_to(analysis)
    aspect_function_analyzer.register_to(analysis)
    doc_coverage_semantic_analyzer.register_to(analysis)
    dryness_analyzer.register_to(analysis)
    project_call_graph_analyzer.register_to(analysis)
    static_test_coverage_analyzer.register_to(analysis)
    commit_db_analyzer.register_at_end(analysis)
    report_writer.register_to(analysis)
    analysis.analyze()
    inspector.dump(project, "{}_{}_{}".format(INSPECTOR_FILENAME, project.repo_name, time.time()))
    print('ok')


if __name__ == '__main__':
    init_log_config(formatter='plain')
    run_ccg()
